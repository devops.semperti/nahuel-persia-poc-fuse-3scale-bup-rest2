package com.semperti.hipotecario.poc.fuse.model;


import java.util.ArrayList;

public class PersonaBup extends Persona{
	protected String valorLealtadCliente;

	public String getValorLealtadCliente() {
		return valorLealtadCliente;
	}
	public void setValorLealtadCliente(String valorLealtadCliente) {
		this.valorLealtadCliente = valorLealtadCliente;
	}

}
