package com.semperti.hipotecario.poc.fuse.model;

import java.util.List;
import java.util.ArrayList;

import java.io.Serializable;

public class TelefonosBup implements Serializable {
	protected List<TelefonoBup> telefonos = new ArrayList<TelefonoBup>();
	public TelefonosBup() {}

	public TelefonosBup(List<TelefonoBup> telefonos) {
		this.telefonos= telefonos;
	}
	
	public List<TelefonoBup> getTelefonos() {
		return telefonos;
	}

	public void setTelefonos(List<TelefonoBup> telefonos) {
		this.telefonos = telefonos;
	}
}
