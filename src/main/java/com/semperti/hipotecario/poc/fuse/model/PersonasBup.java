package com.semperti.hipotecario.poc.fuse.model;

import java.io.Serializable;

import java.util.List;
import java.util.ArrayList;

public class PersonasBup implements Serializable {
	List<PersonaBup> personas = new ArrayList<PersonaBup>();
	public PersonasBup() {}
	
	public PersonasBup(List<PersonaBup> personas) {
		this.personas= personas;
	}
	public List<PersonaBup> getPersonas() {
		return personas;
	}

	public void setPersonas(List<PersonaBup> personas) {
		this.personas = personas;
	}
}
