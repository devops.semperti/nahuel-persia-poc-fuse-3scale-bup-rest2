package com.semperti.hipotecario.poc.fuse.model;

public class TelefonoBup extends Telefono {
	  protected Integer prioridad = null;
	  protected Boolean esListaNegra = null;

	  /**
	   * Orden de prioridad del telefono dentro de los telefonos de la persona
	   * @return prioridad
	  **/
	  public Integer getPrioridad() {
	    return prioridad;
	  }
	  public void setPrioridad(Integer prioridad) {
	    this.prioridad = prioridad;
	  }
	 /**
	   * Indica si el teléfono esta dentro de la lista negra
	   * @return esListaNegra
	  **/
	  public Boolean getEsListaNegra() {
	    return esListaNegra;
	  }
	  public void setEsListaNegra(Boolean esListaNegra) {
	    this.esListaNegra = esListaNegra;
	  }
}
